# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy import log
import beanstalkc
import json

class Baidu1Pipeline(object):
    def __init__(self):
        #beanstalk
        self.beanstalk = beanstalkc.Connection(host='10.213.255.253', port=9090)

    def process_item(self, item, spider):
        self.beanstalk.use('baidu_url_store_game_url'
        log.msg('''id=[%s],type=[%s],name=[%s],url=[%s]''' %str(item['game_id']), str(item['game_type_name']), str(item['game_name']), str(item['game_url'])), level=log.INFO)
        job = {}
        job['id'] = str(item['game_id'])
        job['type'] = str(item['game_type_name'])
        job['name'] = str(item['game_name'])
        job['url'] = str(item['game_url'])
        job['down'] = str(item['game_download_times'])
        job['mark'] = str(item['game_mark_num'])
        job['stars'] = str(item['game_stars'])
        job['update'] = str(item['game_lastupdate'])
        log.msg('''put beanstalk job = [%s]''' %(job), level=log.INFO)
        try:
            self.beanstalk.put(json.dumps(job))
        except Exception, e:
            log.msg('''pipelines process_item exception [%s]''' %(e),level=log.ERROR)
        return item
