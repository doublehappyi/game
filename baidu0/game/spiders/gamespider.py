#!/usr/bin/env python
#coding=utf-8

from scrapy.contrib.spiders import CrawlSpider,Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector,Selector
from game.items import Baidu1GameUrlItem
from scrapy import log
from scrapy.http import FormRequest,Request
import re
import pprint
import time

class Baidu1Spider(CrawlSpider):
    name = 'baidu1'
    allowed_domains = ["shouji.baidu.com"]
    start_urls = ["http://shouji.baidu.//game/?from=as"]
    rules = (
        Rule(SgmlLinkExtractor(allow=('/game/list\?cid=401')),callback="parse_page",follow=False),
        # Rule(SgmlLinkExtractor(allow=('/game/list\?cid=402')),callback='parse_page',follow=False),
    )
    def parse_page(self,response):
        sel = Selector(response)
        page_total =0
        page_num = 0
        if str(response.url).find('page_num') == -1:
            page_total = int(sel.xpath('//div[@class="pager"]/@data-total').extract()[0].encode('utf8','ignore'))
            page_num = 1
            page_next_num = int(page_num) + 1
            page_next_url = str(response.url) + "&page_num=" + str(page_next_num)
        else:
            page_total = response.meta['page_total']
            page_num = response.meta['page_num']
            page_next_num = int(page_num) + 1
            page_next_url = response.url.split('page_num=')[0] + 'page_num' + str(page_next_num)

        if page_next_num <= page_total:
            page_req = Request(page_next_url,callback = self.parse_page)
            page_req.meta['page_num'] = page_next_num
            page_req.meta['page_total'] = page_total
            yield page_req
        else:
            pass

        game_type_name = sel.xpath('//ul[@class="yui3-u type-name"]/li/a[@class="cur"]/text()').extract()[0].encode('utf8','ignore')
        for game in sel.xpath('//div[@class="list-bd app-bd"]/ul'):
                game_name = game.xpath('div[@class="app-box"]//p[@class="name"]/text()').extract()[0].encode('utf8', 'ignore')
                game_url = "http://shouji.baidu.com"+game.xpath('div[@class="app-box"]/a/@href').extract()[0].encode('utf8', 'ignore')
                game_id = str(game_url).split("docid=")[1].split("&")[0]
                game_stars = game.xpath('div[@class="app-box"]//div[@class="app-meta"]//span[@class="star"]/span/@style').extract()[0].encode('utf8', 'ignore').split(":")[1].split("%")[0]
                game_stars = str(float(game_stars)/10/2)
                game_download_times = game.xpath('div[@class="app-box"]//span[@class="down"]/text()').extract()[0].encode('utf8', 'ignore')
                game_download_times = self.convent_string_to_num(game_download_times)

                if game_url:
                    req = Request(game_url, callback=self.get_game_content)
                    req.meta['game_type_name'] = game_type_name
                    req.meta['game_url'] = game_url
                    req.meta['game_name'] = game_name
                    req.meta['game_id'] = game_id
                    req.meta['game_download_times'] = game_download_times
                    req.meta['game_stars'] = game_stars
                    yield req


    def get_game_content(self,response):
        sel = Selector(response)
        game_groupid = sel.xpath('//input[@name="groupid"]/@value').extract()[0].encode('utf8', 'ignore')
        comment_url = "http://shouji.baidu.com/comment?action_type=getCommentList&groupid="+game_groupid
        comment_req = Request(comment_url, callback=self.get_comment_pager_size)
        comment_req.meta["game_type_name"] = response.meta['game_type_name']
        comment_req.meta["game_url"] = response.meta['game_url']
        comment_req.meta["game_name"] = response.meta['game_name']
        comment_req.meta["game_id"] = response.meta['game_id']
        comment_req.meta["game_download_times"] = response.meta['game_download_times']
        comment_req.meta["game_stars"] = response.meta['game_stars']
        return comment_req

    def get_comment_pager_size(self, response):
        sel = Selector(response)
        comment_page_size = sel.xpath('//input[@name="totalpage"]/@value').extract()[0]
        comment_last_url  = str(response.url)+"&pn="+str(comment_page_size)
        comment_mark_req = Request(comment_last_url, callback=self.get_comment_marks)
        comment_mark_req.meta["game_type_name"] = response.meta['game_type_name']
        comment_mark_req.meta["game_url"] = response.meta['game_url']
        comment_mark_req.meta["game_name"] = response.meta['game_name']
        comment_mark_req.meta["game_id"] = response.meta['game_id']
        comment_mark_req.meta["game_download_times"] = response.meta['game_download_times']
        comment_mark_req.meta["game_stars"] = response.meta['game_stars']
        comment_mark_req.meta["comment_page_size"] = str(comment_page_size)
        return comment_mark_req

    def get_comment_marks(self, response):
        sel = Selector(response)
        game_store = Baidu1GameUrlItem()
        game_store['game_type_name'] = response.meta['game_type_name']
        game_store['game_url'] = response.meta['game_url']
        game_store['game_name'] = response.meta['game_name']
        game_store['game_id'] = response.meta['game_id']
        game_store['game_download_times'] = response.meta['game_download_times']
        game_store["game_stars"] = response.meta['game_stars']
        game_store["game_mark_num"] = str(10*(int(response.meta['comment_page_size'])-1)+len(sel.xpath('//ol')))
        game_store["game_lastupdate"] = time.strftime('%Y-%m-%d',time.localtime())
        return game_store

    def convent_string_to_num(self, s):
       if s.isdigit():
           return str
       elif s.find("万") != -1:
           base = float(s.split("万 ")[0])
           return str(base*10000)
       elif s.find("亿 ") != -1:
           base = float(s.split("亿 ")[0])
           return str(base*100000000)
       elif s.find("下 ") != -1:
           base = float(s.split("下 ")[0])
           return str(base)
       else:
           return str(0)













