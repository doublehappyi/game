__author__ = 'Administrator'

import base64
#L5 Router
import l5sys
import datetime
from scrapy import log
# Start your middleware class
class ProxyMiddleware(object):
    def __init__(self):
        self.proxy_list = ["http://10.140.62.33:8088"]
    # overwrite process request
    def process_request(self, request, spider):
        # Set the location of the proxy
        proxy_ip = "127.0.0.1"
        proxy_port = "80"
        try:
            ret,qos = l5sys.ApiGetRoute({'modId':64030017,'cmdId':65536},0.2)
            print "ip:"+qos['hostIp']+",port:"+repr(qos['hostPort'])
            proxy_ip = qos['hostIp']
            proxy_port = repr(qos['hostPort'])
            request.meta['l5ret'] = ret
            request.meta['req_time'] = datetime.datetime.now().microsecond
            request.meta['qos'] = qos
            # log.msg(''' L5 Router Get It l5ret:%s,req_time:%s,qos:%s ''' %(str(request.meta['l5ret']), str(request.meta['req_time']), str(request.meta['qos'])), level=log.INFO)
        except Exception, e:
            pass
            # log.msg(" L5 Router Get Catch A Exception ", level=log.ERROR)

        # log.msg(''' L5 Router Proxy IP:%s,Port:%s ''' %(str(proxy_ip), str(proxy_port)), level=log.INFO)
        request.meta['proxy'] = "http://"+proxy_ip+":"+proxy_port
        # log.msg(''' Proxy:%s ''' %(str(request.meta['proxy'])), level=log.INFO)
        self.proxy_list.insert(0, request.meta['proxy'])
    def process_response(self, request, response, spider):
        iret = request.meta['l5ret']
        use_time = datetime.datetime.now().microsecond - request.meta['req_time']
        qos = request.meta['qos']
        # log.msg(''' Response ret is %s, use_time is %s''' %(str(iret), str(use_time)), level=log.INFO)
        ret = l5sys.ApiRouteResultUpdate(qos,iret,use_time)
        # log.msg(''' ApiRouteResultUpdate ret is %s''' %(str(ret)), level=log.INFO)
        return response

