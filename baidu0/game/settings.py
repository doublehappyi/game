# -*- coding: utf-8 -*-

# Scrapy settings for game project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'game'
SPIDER_MODULES = ['game.spiders']
NEWSPIDER_MODULE = 'game.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'game (+http://www.yourdomain.com)'
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'baidu1 (+http://www.yourdomain.com)'
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'baidu (+http://www.yourdomain.com)'
DOWNLOAD_DELAY = 0.5
#鍚屾椂璇锋眰鏁
#default :16
CONCURRENT_REQUESTS = 10
#缁撴灉ITEM閫氳繃PIPE
ITEM_PIPELINES = {
    'baidu1.pipelines.Baidu1Pipeline':800,
}

DOWNLOADER_MIDDLEWARES = {
    'baidu1.middlewares.ProxyMiddleware': 100,
}

#鏃ュ織鐨勮缃
LOG_ENABLED = True
LOG_ENCODING = 'utf-8'
LOG_FILE = '/data/home/user00/scrapy/log/baidu1.log'
LOG_LEVEL = 'INFO'
LOG_STDOUT = True
