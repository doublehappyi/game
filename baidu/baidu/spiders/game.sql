create table game(
    id int unsigned not null auto_increment primary key,
    game_url varchar(20) not null unique,
    game_type_name varchar(200) unique ,
    game_name varchar(200) not null unique,
    game_id varchar(20) unique,
    game_download_times varchar(200) ,
    game_mark_num varchar(15) ,
    game_stars varchar(20) ,
    game_lastupdate varchar(20)
);