#coding=utf-8
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor
from baidu.items import BaiduGameItem
import time, json

#公共函数，用于从url中提取指定参数值
def url2dict(url):
    params_list = url.split('?')[1].split('&')
    for i, p in enumerate(params_list):
        params_list[i] = tuple(p.split('='))
    return dict(params_list)
#公共函数，字符串中含有万，亿单位的转换：如'12''12万'，'12亿'
#只接受urf-8格式的数据
def unit_conversion(s):
    s = s.strip()
    ret = ''
    try:
        if '万' in s:
            ret = float(s.split('万')[0])*10000
        elif '亿' in s:
            ret = float(s.split('亿')[0])*100000000
        else:
            ret = s
    except:
        ret = s
    return str(ret)
            
    
class BaiduGameSpider(CrawlSpider):
    '''
    爬取流程：
    1，从起始页获取所有的二级分类
    2，在每一个二级分类页获取该分类下的所有翻页地址
    3，在翻页地址中提取每一个游戏地址
    4，在游戏详情页提取游戏数据
    5，在游戏详情页进一步发送request请求，提取出评论数据
    6，在评论页组装item并返回
    '''
    name = 'baidu'
    allowed_domains = ['shouji.baidu.com']
    start_urls = ['http://shouji.baidu.com/game/']
    rules = [Rule(LxmlLinkExtractor(allow=r'/game/list\?cid=(\d+)&boardid=board[_0-9]+'), callback='parse_sub_cate',follow=False),]
    
    #从start_urls提取所有二级分类页地址，并生成相应的Request对象
    def parse_sub_cate(self, response):
        params_dict = url2dict(response.url)
        cid = str(params_dict['cid'])
        boardid = str(params_dict['boardid'])
        pager_total = str(response.css(".pager ::attr('data-total')").extract()[0])
        
        page_num = 1
        while page_num <= int(pager_total):
            page_url = 'http://shouji.baidu.com/game/list?boardid=' + boardid + '&cid=' + cid + '&page_num=' + str(page_num)
            yield Request(page_url, method='GET', callback=self.parse_page)
            page_num += 1
    
    #解析翻页页面的response数据，获取游戏详情页地址，并生成相应的Request对象
    def parse_page(self, response):
        doc_urls = response.css('.app-box > a ::attr(href)').extract()
        for url in doc_urls:
            doc_url = "http://shouji.baidu.com" + url
            yield Request(doc_url, method='GET', callback=self.parse_doc)
    
    #1，解析游戏详情页的response数据，提取游戏数据，并通过request.meta属性传递；
    #2，生成获取该游戏评论的地址以及相应的Request对象
    def parse_doc(self, response):
        url_dict = url2dict(response.url)
        meta = {}
        meta["game_url"] = response.url
        meta["game_type_name"] = response.css(".nav > span > a[href*='/game/list'] ::text").extract()[0]
        meta["game_name"] = response.css(".content-right > .app-name > span ::text").extract()[0]
        meta["game_id"] = url_dict['docid']
        meta["game_download_times"] = unit_conversion(response.css(".detail > .download-num ::text").extract()[0].split(":")[1].encode('utf8', 'ignore'))
        meta["game_mark_num"] = ""
        meta["game_stars"] = response.css(".star-percent ::attr(style)").extract()[0].split(":")[1].strip()
        meta["game_lastupdate"] = time.strftime('%Y-%m-%d',time.localtime())
        
        #这里要获取评论数：因为评论数是通过ajax加载，所以需要另外发送一个request请求
        groupid = response.css("input[name=groupid] ::attr(value)").extract()[0]
        comment_url = "http://shouji.baidu.com/comment?action_type=getCommentList&groupid="+groupid
        yield Request(comment_url, method="GET", callback=self.parse_comment, meta=meta)
    
    #1，解析评论数据
    #2，提取response.meta中的数据，组装item对象并返回
    def parse_comment(self, response):
        meta = response.meta
        item = BaiduGameItem()
        #获取评论总页数
        total_page = int(response.css('input[name=totalpage] ::attr(value)').extract()[0])
        game_mark_num = str(total_page*10)
        
        item["game_mark_num"] = meta["game_mark_num"] 
        item["game_url"] = meta["game_url"]
        item["game_type_name"] = meta["game_type_name"]
        item["game_name"] = meta["game_name"] 
        item["game_id"] = meta["game_id"]
        item["game_download_times"] = meta["game_download_times"]
        item["game_mark_num"] = game_mark_num
        item["game_stars"] = meta["game_stars"]
        item["game_lastupdate"] = meta["game_lastupdate"]
        yield item
        
        