# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field

class CateItem(Item):
    cate_url = Field()
    
class BoardItem(Item):
    board_url = Field()

class PageItem(Item):
    page_url = Field()
    
class DocItem(Item):
    doc_url = Field()
   
class BaiduGameItem(Item):
    game_url = Field()
    game_type_name = Field()
    game_name = Field()
    game_id = Field()
    game_download_times = Field()
    game_mark_num = Field()
    game_stars = Field()
    game_lastupdate = Field()

class BaiduItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
